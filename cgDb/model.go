package cgDb

type ProcedureResult struct {
	Result  int    `gorm:"type:int(11);"`
	Message string `gorm:"type:varchar(128);"`
	Assist  string `gorm:"type:varchar(1024);"`
}
