package cgUtils

import "time"

const DATETIME_FORMT = "2006-01-02 15:04:05"

func DateFromString(timeStr string) (time.Time, error) {
	return time.Parse(DATETIME_FORMT, timeStr)
}
