package cgUtils

import (
	"encoding/json"
)

func SliceStructToInterface[T any](s []T) []interface{} {
	var interfaceSlice []interface{} = make([]interface{}, len(s))
	for i, d := range s {
		interfaceSlice[i] = d
	}
	return interfaceSlice
}

func SliceStructToJson[T any](s []T) [][]byte {
	var stringSlice [][]byte = make([][]byte, len(s))
	for i, d := range s {
		jsonString, err := json.Marshal(d)
		if err != nil {
			continue
		}
		stringSlice[i] = jsonString
	}
	return stringSlice
}

// sort
func SliceSort[T any](ary []T, fn func(a T, b T) int) {
	l := len(ary)
	for i := 0; i < l; i++ {
		for j := i; j > 0; j-- {
			if fn(ary[j], ary[j-1]) < 0 {
				_t := ary[j]
				ary[j] = ary[j-1]
				ary[j-1] = _t
			}
		}
	}
}
