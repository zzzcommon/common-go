package cgUtils

func MapClear[K string | int, V any](m *map[K]V) {
	for k := range *m {
		delete(*m, k)
	}
}
