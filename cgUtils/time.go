package cgUtils

import (
	"fmt"
	"time"
)

// 函数中用defer就行
func TimeCost(tip string, start time.Time) {
	tc := time.Since(start)
	fmt.Printf("%s -> time cost = %v\n", tip, tc)
}
