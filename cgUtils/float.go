package cgUtils

import (
	"fmt"
	"strconv"
)

func ParseFloat(value float64) float64 {
	rs, err := strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	if err != nil {
		return value
	}
	return rs
}
