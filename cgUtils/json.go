package cgUtils

import (
	"encoding/json"
	"os"
)

func JsonFromFile[T any](filePath string) (*T, error) {
	conf, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	t := new(T)
	err = json.Unmarshal(conf, t)
	if err != nil {
		return nil, err
	}
	return t, nil
}

func JsonToFile(v any, filePath string) error {
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	return json.NewEncoder(file).Encode(v)
}
