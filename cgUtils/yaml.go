package cgUtils

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

func ReadYamlConfig(filePath string, conf interface{}) (err error) {
	yamlFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(yamlFile, conf)
}

func ReadYamlConfigs(conf interface{}, filePaths ...string) {
	for _, filePath := range filePaths {
		yamlFile, err := ioutil.ReadFile(filePath)
		if err != nil {
			continue
		}
		yaml.Unmarshal(yamlFile, conf)
	}
}
