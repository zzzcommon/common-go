package cgUtils

import "encoding/json"

func Ptr[T any](obj T) *T {
	return &obj
}

func Map2Struct(m any, s any) {
	b, _ := json.Marshal(m)
	_ = json.Unmarshal(b, s)
}
