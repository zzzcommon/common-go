package cgUtils

import (
	"github.com/golang-jwt/jwt/v5"
)

// payload可以传对象可以传MapClaims。
func JwtCreateToken(payload jwt.Claims, secret string) (string, error) {
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, payload)
	token, err := at.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}
	return token, nil
}

// 只能返回MapClaims，需要对象自己弄吧。
func JwtParseToken(token string, secret string) (jwt.MapClaims, error) {
	claim, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}
	return claim.Claims.(jwt.MapClaims), nil
}
