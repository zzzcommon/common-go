package cgUtils

import "testing"

func TestSliceSort(t *testing.T) {
	var ary = make([]int, 0)
	ary = append(ary, 4, 3, 5, 2, 6, 1)
	SliceSort(ary, func(a, b int) int {
		if a > b {
			return 1
		} else {
			return -1
		}
	})
	t.Log(ary)
}
