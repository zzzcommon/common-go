package cgHttp

import "testing"

func TestPost(t *testing.T) {
	client := &HttpClient{}
	params := &PostParams{
		Url:  "https://ggzy.guizhou.gov.cn/tradeInfo/es/list",
		Body: []byte(`{"channelId":"5904475","pageNum":1,"pageSize":20}`),
	}
	rs := client.Post(params)
	if rs.Err != nil {
		t.Fatal(rs.Err)
	}
	if rs.StatusCode != 200 {
		t.Fatalf("status code: %d", rs.StatusCode)
	}
	t.Log(rs.BodyString)
}
