package cgModel

type NullType struct {
}

var _null *NullType

func init() {
	_null = &NullType{}
}

func Null() *NullType {
	return _null
}
