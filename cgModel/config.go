package cgModel

import "fmt"

// * app *
type Application struct {
	Port   string `yaml:"port"`
	Dev    bool   `yaml:"dev"`
	Server string `yaml:"server"`
}

// * db *
type DbConfig struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Schema   string `yaml:"schema"`
}

// * nsq *
type NsqConfig struct {
	NsqLookupds  *[]string `yaml:"nsq_lookupd_addrs"`
	NsqdTcpAddr  *string   `yaml:"nsqd_addr"`
	NsqdHttpAddr *string   `yaml:"nsqd_http_addr"`
}

func (it NsqConfig) String() string {
	nsqLookupdsString := ""
	for _, nsqLookupd := range *it.NsqLookupds {
		nsqLookupdsString += nsqLookupd
	}
	return fmt.Sprintf("NsqConfig:{NsqLookupds:[%s] NsqdTcpAddr:%s NsqdHttpAddr:%s}", nsqLookupdsString, *it.NsqdTcpAddr, *it.NsqdHttpAddr)
}

// * redis *
type RedisConfig struct {
	Address  string `yaml:"addr"`
	Password string `yaml:"password"`
	Db       int    `yaml:"db"`
}
